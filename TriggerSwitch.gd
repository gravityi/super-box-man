extends Area2D

var enabled = true

func _ready():
	disable_tiles(get_node("TriggerTiles" + str(int(!enabled))))

func _on_TriggerSwitch_body_entered(body):
	enable_tiles(get_node("TriggerTiles" + str(int(!enabled))))
	disable_tiles(get_node("TriggerTiles" + str(int(enabled))))
	enabled = !enabled

func enable_tiles(tilemap):
	tilemap.collision_layer = 1
	tilemap.collision_mask = 3
	tilemap.modulate.a = 1

func disable_tiles(tilemap):
	tilemap.collision_layer = 0
	tilemap.collision_mask = 0
	tilemap.modulate.a = 0.5

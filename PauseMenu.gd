extends PopupMenu

func _process(delta):
	if (Input.is_action_just_pressed("pause") and !get_tree().paused) or (get_tree().paused and !visible and !$"../SettingsPopup".visible):
		popup_centered()
		get_tree().paused = true
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()

func unpause():
	get_tree().paused = false

func _on_PauseMenu_index_pressed(index):
	match index:
		0:
			unpause()
			hide()
		1:
			unpause()
			Globals.player_dead = true
			get_tree().reload_current_scene()
		2:
			unpause()
			get_tree().change_scene("res://LevelSelect.tscn")
		3:
			$"../SettingsPopup".call_deferred("popup_centered")
			hide()
		4:
			pass
		5:
			get_tree().quit()

extends Node

export var decay = 0.8  # How quickly the shaking stops [0, 1].
export var max_offset = Vector2(100, 75)  # Maximum hor/ver shake in pixels.
export var max_roll = 0.1  # Maximum rotation in radians (use sparingly).

var trauma = 0.0  # Current shake strength.
var trauma_power = 2  # Trauma exponent. Use [2, 3].
var noise_y = 0

onready var Cam = get_parent()
onready var noise = OpenSimplexNoise.new()

func add_trauma(shake_amount):
	trauma = min(trauma + shake_amount, 1.0)

func shake_camera(amount):
	trauma = 0
	add_trauma(amount)

func shake():
	var amount = pow(trauma, trauma_power)
	noise_y += 1
	Cam.rotation = max_roll * amount * noise.get_noise_2d(noise.seed, noise_y)
	Cam.offset.x = max_offset.x * amount * noise.get_noise_2d(noise.seed*2, noise_y)
	Cam.offset.y = max_offset.y * amount * noise.get_noise_2d(noise.seed*3, noise_y)

func _ready():
	randomize()
	noise.seed = randi()
	noise.period = 4
	noise.octaves = 2

func _process(delta):
	if trauma != 0:
		trauma = max(trauma - decay * delta, 0)
		shake()
	$"..".global_position = $"../../Player".global_position


extends Node

signal timeout

export(float) var wait_time
export(bool) var one_shot = false
export(bool) var autostart = false

var time_left = 0
var is_stopped = true
var paused = false

func _ready():
	if autostart: is_stopped = false

func _process(delta):
	if !is_stopped and !paused:
		time_left += delta * Globals.speed_scale
		if time_left >= wait_time:
			emit_signal("timeout")
			time_left = 0
			if one_shot: is_stopped = true

func start():
	time_left = 0
	is_stopped = false

func stop():
	time_left = 0
	is_stopped = true

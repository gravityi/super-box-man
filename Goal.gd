extends Area2D

export(String) var sub_folder
export(String) var next_level
export(Shape2D) var shape
export(Texture) var texture
export(float) var bob_frequency = 0.1
export(float) var bob_variation = 15

var starting_y
var bob_direction = 1
var time = 0

func _ready():
	if shape != null: $"CollisionShape2D".shape = shape
	if texture != null: $"Sprite".texture = texture
	starting_y = position.y

func _on_Goal_body_entered(body):
	if body.is_in_group("player"):
		Globals.player_won = true
		body.visibility_notifier_enabled = false
		get_tree().change_scene("res://levels/" + sub_folder + "/" + next_level +".tscn")

func _process(delta):
	bob_animation(delta)

func bob_animation(delta):
	time += delta * bob_frequency * Globals.speed_scale
	position.y = bob_variation * sin(time * PI * 10) + starting_y

extends KinematicBody2D

export(float) var speed = 100

var points
var target
var direction

func _ready():
	update_point_list()

func _process(delta):
	if !Globals.player_dead:
		if global_position.distance_to(target.global_position) <= 1:
			push_back(target)
			update_point_list()
		move_and_collide(direction * speed * Globals.speed_scale * delta)

func push_back(node):
	node.get_parent().move_child(node, $"Points".get_child_count())

func update_point_list():
	points = $"Points".get_children()
	target = points[0]
	direction = global_position.direction_to(target.global_position).normalized()

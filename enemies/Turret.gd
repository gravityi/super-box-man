extends StaticBody2D

const Bullet = preload("res://enemies/EnemyBullet.tscn")

export(float) var bullet_speed = 1
export(float) var fire_interval = 1
export(Texture) var bullet_texture
export(bool) var takes_damage = false
export(int) var health = 1

onready var Player = $"../Player"

func _ready():
	$"ShotTimer".wait_time = fire_interval

func shoot():
	var NewBullet = Bullet.instance()
	NewBullet.get_node("Sprite").texture = bullet_texture
	NewBullet.get_node("Light2D").texture = bullet_texture
	NewBullet.velocity = Vector2(bullet_speed, 0)
	NewBullet.global_position = $"BulletSpawn".global_position
	NewBullet.rotation_angle = rotation
	get_tree().get_root().get_child(1).add_child(NewBullet)

func _on_ShotTimer_timeout():
	if !Globals.player_dead: shoot()

func take_damage(amount):
	if takes_damage:
		health -= amount
		if health <= 0:
			queue_free()

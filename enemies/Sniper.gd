extends StaticBody2D

export var bullet_speed = 5
export(Texture) var weapon_texture
export(Texture) var bullet_texture
export(String) var particle_type
export(bool) var takes_damage = true
export(int) var health = 1

var player_in = false

onready var Player = $"../Player"

func _on_DetectionRadius_body_entered(body):
	if body.is_in_group("player"):
		player_in = true
		$"ShotTimer".start()

func _on_DetectionRadius_body_exited(body):
	if body.is_in_group("player"):
		player_in = false
		$"ShotTimer".stop()

func _process(delta):
	if Globals.player_dead:
		$"ShotTimer".stop()
	elif player_in:
		$"EnemyWeapon".look_at(Player.global_position)

func _on_ShotTimer_timeout():
	$"EnemyWeapon".shoot()

func take_damage(amount):
	health -= amount
	if health <= 0:
		queue_free()

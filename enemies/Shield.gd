extends Area2D

const Bullet = preload("res://enemies/EnemyBullet.tscn")

func _on_Shield_body_entered(body):
	if body.is_in_group("player_bullet"):
		var NewBullet = Bullet.instance()
		NewBullet.get_node("Sprite").texture = body.get_node("Sprite").texture
		NewBullet.get_node("Light2D").texture = body.get_node("Sprite").texture
		NewBullet.modulate = $"../..".bullet_color
		NewBullet.velocity = body.position.direction_to(get_tree().get_root().get_child(1).get_node("Player").position) * $"../..".bullet_speed
		NewBullet.position = body.position
		NewBullet.get_node("Sprite").look_at(body.position.direction_to(get_tree().get_root().get_child(1).get_node("Player").position))
		get_tree().get_root().get_child(1).call_deferred("add_child", NewBullet)
		body.call_deferred("queue_free")
		$"../../DefenseTimer".start()
		$"../../".can_defend = false
	if body.is_in_group("player") and visible:
		body._on_player_death()

extends StaticBody2D

export(float) var defense_delay
export(float) var bullet_speed
export(Color) var bullet_color
export(bool) var takes_damage = true
export(int) var health = 1

var can_defend = true

func _ready():
	$"DefenseTimer".wait_time = defense_delay

func _process(delta):
	if !$"DefenseTimer".is_stopped() and $"../Player".can_move:
		$"DefenseTimer".stop()
		_on_DefenseTimer_timeout()

func _on_DefenseTimer_timeout():
	$"ShieldPivot/Shield".hide()
	can_defend = true

func _on_DetectionRadius_body_entered(body):
	if body.is_in_group("player_bullet") and can_defend:
		$"ShieldPivot".look_at(body.global_position)
		$"ShieldPivot/Shield".show()

func take_damage(amount):
	health -= amount
	if health <= 0:
		queue_free()

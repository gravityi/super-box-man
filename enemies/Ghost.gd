extends Area2D

export(float) var speed
export(bool) var takes_damage = true
export(int) var health = 1

func _process(delta):
	if $"../Player".can_move and !Globals.player_dead: position += global_position.direction_to($"../Player".global_position) * delta * speed

func _on_Ghost_body_entered(body):
	if body.is_in_group("player"): body._on_player_death()
	elif body.is_in_group("player_bullet"):
		take_damage(body.damage)
		body.queue_free()
	
func take_damage(amount):
	health -= amount
	if health <= 0:
		queue_free()

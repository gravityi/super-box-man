extends KinematicBody2D

var velocity = Vector2()
var rotation_angle = 0

func _ready():
	velocity = velocity.rotated(rotation_angle)
	rotation = rotation_angle

func _process(delta):
	if !Globals.player_dead:
		var collision = move_and_collide(velocity * Globals.speed_scale)
		if collision != null:
			if !collision.collider.is_in_group("player") and !collision.collider.is_in_group("bullet") and !collision.collider.is_in_group("shield"):
				queue_free()
			elif collision.collider.is_in_group("player"):
				collision.collider._on_player_death()

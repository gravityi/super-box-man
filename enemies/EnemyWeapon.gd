extends Node2D

const Bullet = preload("res://enemies/EnemyBullet.tscn")

var particle

func _ready():
	$"Sprite".texture = get_parent().weapon_texture
	particle = load("res://particles/" + get_parent().particle_type + ".tscn")

func shoot():
	var NewBullet = Bullet.instance()
	NewBullet.get_node("Sprite").texture = get_parent().bullet_texture
	NewBullet.get_node("Light2D").texture = get_parent().bullet_texture
	NewBullet.velocity = Vector2(get_parent().bullet_speed, 0)
	NewBullet.global_position = $"BulletSpawn".global_position
	NewBullet.rotation_angle = rotation
	get_tree().get_root().get_child(1).add_child(NewBullet)
	var NewParticle = particle.instance()
	NewParticle.emitting = true
	NewParticle.rotation = rotation
	NewParticle.global_position = get_parent().global_position
	get_tree().get_root().get_child(1).add_child(NewParticle)

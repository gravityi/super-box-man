extends Area2D

const Bullet = preload("res://Bullet.tscn")

export(float) var recoil = 3
export(float) var bullet_speed = 5
export(int) var bullet_damage = 1
export(int) var shot_limit = 1
export(float) var fire_rate = 1
export(bool) var is_auto = false
export(Texture) var weapon_texture
export(Texture) var bullet_texture
export(String) var particle_type
export(float) var bob_frequency = 0.1
export(float) var bob_variation = 15

var particle
var starting_y
var time = 0
var held = false

func _ready():
	$"Sprite".texture = weapon_texture
	particle = load("res://particles/" + particle_type + ".tscn")
	starting_y = position.y

func _process(delta):
	for body in get_overlapping_bodies():
		if body.is_in_group("player"):
			if body.get_node("WeaponPivot").get_child_count() == 0:
				var NewWeapon = duplicate()
				body.get_node("WeaponPivot").add_child(NewWeapon)
				NewWeapon.position = body.get_node("WeaponPivot").position
				NewWeapon.held = true
				if 1/fire_rate > 0: body.get_node("FireRateTimer").wait_time = 1/fire_rate
				body.auto = is_auto
				body.get_node("Sounds/Reload").play()
				queue_free()
	if !held: bob_animation(delta)

func shoot():
	var NewBullet = Bullet.instance()
	NewBullet.get_node("Sprite").texture = bullet_texture
	NewBullet.get_node("Light2D").texture = bullet_texture
	NewBullet.velocity = Vector2(bullet_speed, 0)
	NewBullet.global_position = $"BulletSpawn".global_position
	NewBullet.rotation_angle = get_parent().rotation
	NewBullet.damage = bullet_damage
	get_tree().get_root().get_child(1).add_child(NewBullet)
	var NewParticle = particle.instance()
	NewParticle.emitting = true
	NewParticle.rotation = get_parent().rotation
	
#	Enable this and disable both lines below it to have the particle follow the player
#	NewParticle.global_position = get_parent().position
#	get_parent().add_child(NewParticle)
	NewParticle.global_position = get_parent().global_position
	get_tree().get_root().get_child(1).add_child(NewParticle)

func bob_animation(delta):
	time += delta * bob_frequency * Globals.speed_scale
	position.y = bob_variation * sin(time * PI * 10) + starting_y

extends Node2D

onready var Player = $"../Player"

func _process(delta):
	update()
	
func ballistics():
	var initial_velocity = (get_viewport().get_mouse_position() - get_viewport().size/2).clamped(1)
	var current_position = Player.position
	var gravity_weight = 0
	for point_index in range(100):
		current_position += initial_velocity * point_index * 2 -  Player.position
		initial_velocity += Vector2(0, gravity_weight)
		gravity_weight += 9.8 * point_index * 0.0005
		if floor(point_index/5) == ceil(point_index/5): draw_circle(current_position + Player.position, 5, Color(0, 0, 1))

func _draw():
	ballistics()

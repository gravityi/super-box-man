extends KinematicBody2D

const no_cursor = preload("res://assets/no_cursor.png")
const aim_cursor = preload("res://assets/aim_cursor.png")
const FloatingText = preload("res://FloatingText.tscn")

export var range_constant = 1000
export var gravity = 8
export var global_speed = 1
export var speed_scale = 0.5
export var speed_clamp = 1.5
export var aim_scale_percentage = 0.2
export var shake_on_contact = 0.2
export var shake_on_shot = 0.4
export var drag = 0.5

var velocity = Vector2()
var gravity_accel = 0
var can_move = false
var can_shoot = true
var aiming = false
var launch_pressed = false
var delta_time = 0
var shot_count = 0
var auto = false
var visibility_notifier_enabled = false

onready var AnimPlayer = $"Sprite/AnimationPlayer"

func _ready():
	Input.set_custom_mouse_cursor(aim_cursor, 0, Vector2(15, 15))
	Globals.speed_scale = global_speed
	Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
	if Globals.player_dead:
		$"../Camera2D/Screenshake".shake_camera(shake_on_shot)
		Globals.player_dead = false
		$"Sounds/Restart".play()
	if Globals.player_won:
		$"Sounds/Win".play()
		Globals.player_won = false
	$"UICanvas/RestartText".hide()
	visibility_notifier_enabled = true

func _process(delta):
	update()
	if !Globals.player_dead:
		if Input.is_action_just_pressed("launch") and !aiming:
			launch_pressed = true
			$"CoyoteTimer".start()
		if launch_pressed == true and can_move:
			launch_pressed = false
			gravity_accel = 0
			velocity = ((get_global_mouse_position() - global_position) * delta/2).clamped(speed_clamp)
			can_move = false
			AnimPlayer.play("Airborn")
			$"Sounds/Jump".play()
		elif !can_move:
			velocity += Vector2(0, gravity_accel) * Globals.speed_scale
			gravity_accel += gravity * delta * Globals.speed_scale * speed_scale/ range_constant
		var collision = move_and_collide(velocity * Globals.speed_scale * speed_scale )
		if collision != null:
			if collision.collider.is_in_group("sticky"):
				AnimPlayer.play("Idle")
				can_move = true
				velocity = Vector2()
				if shot_count != 0: $"Sounds/Reload".play()
				shot_count = 0
				aiming = false
				Globals.speed_scale = global_speed
				$"Sprite".rotation = collision.normal.angle() + PI/2
				$"Sounds/Heartbeat".stop()
			elif collision.collider.is_in_group("hazard"):
				_on_player_death()
		if Input.is_action_just_pressed("aim") and !can_move and $"WeaponPivot".get_child_count() > 0:
			aiming = true
			Globals.speed_scale = global_speed * aim_scale_percentage
			Input.set_custom_mouse_cursor(aim_cursor)
			AnimPlayer.play("Aiming")
			$"Sounds/Heartbeat".play()
		if Input.is_action_just_released("aim") and !can_move and $"WeaponPivot".get_child_count() > 0:
			aiming = false
			Globals.speed_scale = global_speed
			AnimPlayer.play("Airborn")
	#		Input.set_custom_mouse_cursor(no_cursor)
			$"Sounds/Heartbeat".stop()
		if aiming:
			$"WeaponPivot".look_at(get_global_mouse_position())
			if shot_count < $"WeaponPivot".get_child(0).shot_limit and ((auto and Input.is_action_pressed("launch") and can_shoot) or (!auto and Input.is_action_just_pressed("launch"))):
				can_shoot = false
				$"FireRateTimer".start()
				$"../Camera2D/Screenshake".shake_camera(shake_on_shot)
				$"WeaponPivot".get_child(0).shoot()
				gravity_accel = 0
				shot_count += 1
				if $"WeaponPivot".get_child(0).recoil != 0: velocity = Vector2(-$"WeaponPivot".get_child(0).recoil, 0).rotated($"WeaponPivot".rotation) * Globals.speed_scale
				var NewFloatingText = FloatingText.instance()
				NewFloatingText.global_position = $"TextSpawnPosition".global_position
				NewFloatingText.set_text(str($"WeaponPivot".get_child(0).shot_limit - shot_count))
				get_tree().get_root().get_child(0).add_child(NewFloatingText)
				$"Sounds/Shot".play()
	elif Globals.player_dead and Input.is_action_just_pressed("launch"):
		get_tree().reload_current_scene()
#	Test Only
#	if Input.is_action_just_pressed("ui_cancel"):
#		get_tree().quit()
	if Input.is_action_just_pressed("reset"):
		get_tree().reload_current_scene()
	if Input.is_action_just_pressed("level_select"):
		get_tree().change_scene("res://LevelSelect.tscn")

func ballistics():
	if can_move and !aiming:
		var initial_velocity = (get_global_mouse_position() - global_position).clamped(speed_clamp)
		var current_position = position
		var gravity_weight = 0
		for point_index in range(5):
			current_position = initial_velocity * point_index * (get_global_mouse_position() - global_position).length()/10
			initial_velocity += Vector2(0, gravity_weight)
			gravity_weight += gravity * (point_index / range_constant)
			draw_circle(current_position, 7, Color(0, 0, 0))
			draw_circle(current_position, 4, Color(1, 1, 1))

func _draw():
	ballistics()

func _on_CoyoteTimer_timeout():
	launch_pressed = false

func _on_FireRateTimer_timeout():
	can_shoot = true

func _on_VisibilityNotifier2D_screen_exited():
		if visibility_notifier_enabled: _on_player_death()

func _on_player_death():
	if !Globals.player_dead:
		$"Sounds/Death".play()
		$"TrailPivot/Trail".points = PoolVector2Array()
		Globals.player_dead = true
		$"UICanvas/RestartText".show()
		if $"../Camera2D":
			$"../Camera2D/CameraAnimation".play("death")

func kill_player():
	Globals.player_dead = true

extends Line2D

var target
var point
export var length = 0
var current_length = 0
onready var Player = $"../.."

func _ready():
	point = Player.global_position
	add_point(point)
	
func _process(delta):
	current_length = length/Globals.speed_scale
	global_position = Vector2()
	global_rotation = 0
	point = Player.global_position
	if get_point_count() > 0:
		if point.distance_to(points[get_point_count() - 1]) > 2:
			add_point(point)
		if get_point_count() > current_length or (Player.can_move and get_point_count() > 2):
			remove_point(0)

extends Area2D

var enabled = true

func _on_TriggerButton_body_entered(body):
	if enabled:
		$"TriggerTiles".collision_layer = 0
		$"TriggerTiles".collision_mask = 0
		$"TriggerTiles".modulate.a = 0.5
		enabled = false
